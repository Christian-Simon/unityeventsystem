﻿// EventSystemCore - AEvent.cs
// 
// Created by Christian Simon
// 
// Date 2016-07-30

namespace EventSystem
{
    /// <summary>
    ///     Represents the abstract base class for all events.
    /// </summary>
    public abstract class AEvent
    {
    }
}