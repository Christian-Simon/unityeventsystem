﻿// EventSystemCore - EventSystem.cs
// 
// Created by Christian Simon
// 
// Date 2016-07-30

using System;
using System.Collections.Generic;
using System.Threading;
using EventSystem.Events;

namespace EventSystem
{
    /// <summary>
    ///     Represents the core of the event system.
    /// </summary>
    public class EventSystem
    {
        #region Delegates

        /// <summary>
        ///     Delegate used in public space.
        /// </summary>
        /// <typeparam name="T">Type of AEvent.</typeparam>
        /// <param name="e">The event.</param>
        public delegate void EventDelegate<in T>(T e) where T : AEvent;

        #endregion

        #region Static Variables and Constants

        /// <summary>
        ///     Single instance of EventSystem class.
        /// </summary>
        private static EventSystem instance;

        #endregion

        #region Variables

        /// <summary>
        ///     List containing all delayed invokes.
        /// </summary>
        private readonly List<Timer> delayedInvokes = new List<Timer>(0);

        /// <summary>
        ///     Delegate lookup table.
        /// </summary>
        private readonly Dictionary<Delegate, EventDelegate> delegateLookup = new Dictionary<Delegate, EventDelegate>(0);

        /// <summary>
        ///     Dictionary containing the delegates.
        /// </summary>
        private readonly Dictionary<Type, EventDelegate> delegates = new Dictionary<Type, EventDelegate>(0);

        #endregion

        #region Properties

        /// <summary>
        ///     Returns the single instance of the event system otherwise it will be created.
        /// </summary>
        public static EventSystem Instance
        {
            get
            {
                if (instance != null)
                {
                    return instance;
                }

                instance = new EventSystem();

                return instance;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Default constructor.
        /// </summary>
        private EventSystem()
        {
            AddListener<ApplicationQuitRequest>(OnApplicationQuitRequest);
        }

        #endregion

        #region Functions

        /// <summary>
        ///     Adds a listener to the event system.
        /// </summary>
        /// <typeparam name="T">Type of AEvent.</typeparam>
        /// <param name="del">The function which will be used by the delegate.</param>
        public void AddListener<T>(EventDelegate<T> del) where T : AEvent
        {
            if (delegateLookup.ContainsKey(del))
            {
                return;
            }

            EventDelegate internalDelegate = e => del((T) e);

            delegateLookup[del] = internalDelegate;

            EventDelegate tempDel;

            if (delegates.TryGetValue(typeof(T), out tempDel))
            {
                delegates[typeof(T)] = (tempDel + internalDelegate);
            }
            else
            {
                delegates[typeof(T)] = internalDelegate;
            }
        }

        /// <summary>
        ///     Invokes a specific event.
        /// </summary>
        /// <param name="e">The actual event.</param>
        public void Invoke(AEvent e)
        {
            EventDelegate del;

            if (delegates.TryGetValue(e.GetType(), out del))
            {
                del.Invoke(e);
            }
        }

        /// <summary>
        ///     Invokes a specific event delayed.
        /// </summary>
        /// <param name="e">The actual event.</param>
        /// <param name="delayInSeconds">The time the invoking is delayed.</param>
        public void InvokeDelayed(AEvent e, float delayInSeconds)
        {
            EventDelegate del;

            if (delegates.TryGetValue(e.GetType(), out del))
            {
                Timer timer = null;
                timer = new Timer(obj =>
                {
                    del.Invoke(e);

                    // ReSharper disable AccessToModifiedClosure
                    delayedInvokes.Remove(timer);

                    // ReSharper disable once PossibleNullReferenceException
                    timer.Dispose();
                    // ReSharper restore AccessToModifiedClosure
                }, null, (int) (delayInSeconds * 1000), Timeout.Infinite);

                delayedInvokes.Add(timer);
            }
        }

        /// <summary>
        ///     Removes a listener from the event system.
        /// </summary>
        /// <typeparam name="T">Type of AEvent.</typeparam>
        /// <param name="del">The function which will be removed from the delegate.</param>
        public void RemoveListener<T>(EventDelegate<T> del) where T : AEvent
        {
            EventDelegate internalDelegate;

            if (delegateLookup.TryGetValue(del, out internalDelegate))
            {
                EventDelegate tempDel;

                if (delegates.TryGetValue(typeof(T), out tempDel))
                {
                    // ReSharper disable once DelegateSubtraction 
                    tempDel -= internalDelegate;

                    if (tempDel == null)
                    {
                        delegates.Remove(typeof(T));
                    }
                    else
                    {
                        delegates[typeof(T)] = tempDel;
                    }
                }

                delegateLookup.Remove(del);
            }
        }

        /// <summary>
        ///     Processes the application quit request.
        /// </summary>
        /// <param name="e">The actual event.</param>
        private void OnApplicationQuitRequest(ApplicationQuitRequest e)
        {
            for (var index = 0; index < delayedInvokes.Count; ++index)
            {
                delayedInvokes[index].Dispose();
                delayedInvokes[index] = null;
            }

            delayedInvokes.Clear();
        }

        #endregion

        #region Nested type: EventDelegate

        /// <summary>
        ///     Delegate used in private space.
        /// </summary>
        /// <param name="e">The event.</param>
        private delegate void EventDelegate(AEvent e);

        #endregion
    }
}