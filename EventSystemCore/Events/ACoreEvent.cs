﻿// EventSystemCore - ACoreEvent.cs
// 
// Created by Christian Simon
// 
// Date 2016-09-06

namespace EventSystem.Events
{
    /// <summary>
    ///     Represents the abstract base class used for all core related events.
    /// </summary>
    public abstract class ACoreEvent : AEvent
    {
    }

    /// <summary>
    ///     Represents the application quit request.
    /// </summary>
    public class ApplicationQuitRequest : ACoreEvent
    {
    }
}