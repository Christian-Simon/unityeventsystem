﻿// EventSystem.CSharp - EventDispatcher.cs
// 
// Created by Christian Simon
// 
// Date 2016-07-30

using System;
using Events;
using EventSystem.Events;
using JetBrains.Annotations;
using UnityEngine;

/// <summary>
///     Test class for dispatching events.
/// </summary>
public class EventDispatcher : MonoBehaviour
{
    #region Functions

    /// <summary>
    ///     Continuously invokes a testing function call.
    /// </summary>
    private void OnEnable()
    {
        InvokeRepeating("DispatchTimeMessage", 0.0f, 2.0f);
    }

    /// <summary>
    ///     Cancels the continuously invoked testing function call.
    /// </summary>
    private void OnDisable()
    {
        CancelInvoke("DispatchTimeMessage");
    }

    /// <summary>
    ///     Unity callback.
    ///     Sends a special request to stop all upcoming event invokes when the application gets quit.
    /// </summary>
    private void OnApplicationQuit()
    {
        EventSystem.EventSystem.Instance.Invoke(new ApplicationQuitRequest());
    }

    /// <summary>
    ///     Performs a delayed event invoke.
    /// </summary>
    [UsedImplicitly]
    private void DispatchTimeMessage()
    {
        var timeMessage = DateTime.Now.ToLongTimeString();

        Debug.LogWarning("Dispatching at " + timeMessage);

        EventSystem.EventSystem.Instance.InvokeDelayed(new LogStringMessageRequest(timeMessage), 5.0f);
    }

    #endregion
}