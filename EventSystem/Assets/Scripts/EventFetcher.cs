﻿// EventSystem.CSharp - EventFetcher.cs
// 
// Created by Christian Simon
// 
// Date 2016-07-30

using System;
using Events;
using UnityEngine;

/// <summary>
///     Test class for fetching events.
/// </summary>
public class EventFetcher : MonoBehaviour
{
    #region Functions

    /// <summary>
    ///     Adds a listener to the event system when the game object gets enabled.
    /// </summary>
    private void OnEnable()
    {
        EventSystem.EventSystem.Instance.AddListener<LogStringMessageRequest>(OnLogStringMessageRequest);
    }

    /// <summary>
    ///     Removes the listener from the event system when the game object gets disabled.
    /// </summary>
    private void OnDisable()
    {
        EventSystem.EventSystem.Instance.RemoveListener<LogStringMessageRequest>(OnLogStringMessageRequest);
    }

    /// <summary>
    ///     Processes the log string message request.
    /// </summary>
    /// <param name="e">The actual event.</param>
    private void OnLogStringMessageRequest(LogStringMessageRequest e)
    {
        var timeMessage = DateTime.Now.ToLongTimeString();

        Debug.Log("Fetching message " + e.Message + " (time is " + timeMessage + ")");
    }

    #endregion
}