﻿// EventSystem.CSharp - ALogEvent.cs
// 
// Created by Christian Simon
// 
// Date 2016-07-30

using EventSystem;

namespace Events
{
    /// <summary>
    ///     Represents the abstract base class used for all log related events.
    /// </summary>
    public abstract class ALogEvent : AEvent
    {
    }

    /// <summary>
    ///     Represents the log string message request.
    /// </summary>
    public class LogStringMessageRequest : ALogEvent
    {
        #region Properties

        /// <summary>
        ///     Log message.
        /// </summary>
        public string Message { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Event constructor.
        /// </summary>
        /// <param name="message">Log message.</param>
        public LogStringMessageRequest(string message)
        {
            Message = message;
        }

        #endregion
    }
}